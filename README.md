micloudconfigserver
===================

MI Cloud Configuration Server using Spring.


To use a keystore for encryption you must have the JCE Unlimited policies installed.  For instructions you can see here: 
http://bigdatazone.blogspot.com/2014/01/mac-osx-where-to-put-unlimited-jce-java.html

---
TO MAKE THINGS EASIER I ADDED A START UP SCRIPT THAT CAN BE USED FOR DEVELOPMENT.
You can run the ```./start.sh``` script and if you want to debug use ```./start.sh debug```.

IF YOU WANT TO DEBUG:
    In Intellij open the toolbar and select Run --> Edit Configurations 
    Then you will want to add a new configuration, select + --> Remote
    In the new Remote window you can name the configuration something useful
    Next under Settings, set Transport: Socket, Debugger Mode: Attach, Host: localhost, Port: 5005 (port the start.sh script uses)

---

To run this project you can use this command, the reason we pass in so much data here is the fact that the 
config server cannot decrypt its own properties - and we don't want this sensitve data in plain text config files.
So we do this to ensure we can keep data safe.

     KEYSTORE_PASSWORD=mindsignited KEYSTORE_ALIAS=mindsignited CONFIG_SERVER_PASS=mindsignited EUREKA_SERVER_USER=user EUREKA_SERVER_PASS=mindsignited mvn spring-boot:run

You can pass in another keystore password/alias using an environment variable:

    export KEYSTORE_PASSWORD=mindsignited
    export KEYSTORE_ALIAS=mindsignited
    
You can pass in the security.user.password using an environment variable: 

    export CONFIG_SERVER_PASS=mindsignited

The last thing that needs to be present is the Eureka Server credentials:

    export EUREKA_SERVER_USER=user
    export EUREKA_SERVER_PASS=mindsignited
    
    or
    
    export EUREKA_SERVER_URI=http://user:pass@domain:port

Right now I can only get the git configuration repository to work with master branch.  Also cannot figure out a private repository
credentials to work either. 

You can pass in another git repo to use by using this environment variable, and where to put the repository: 

    export CONFIGSERVER_GIT_REPO=https://github.com/MindsIgnited/springcloudconfig
    export CONFIGSERVER_GIT_BASEDIR=target/config
    
