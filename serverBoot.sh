#!/bin/bash

##
## Startup script for Config Server.  This will set the needed environment variables and
## boot up the application using the spring boot fat jar that is the artifact of the
## production build.
##

CONFIG_APPLICATION=mi-cloud-config-server
CONFIG_APPLICATION_EXTENSION=jar
CONFIG_VERSION=0.0.1-SNAPSHOT
PATH_TO_EXECUTABLE=target # <-- no trailing slash here.
# jvm arguments added here
JVM_OPTS="" #"-Dspring.profiles.active=production"

export KEYSTORE_PASSWORD=mindsignited
export KEYSTORE_ALIAS=mindsignited
export EUREKA_SERVER_URI=http://127.0.0.1:8761/micloudeureka
export SERVER_PORT=8888
export HOSTNAME=127.0.0.1
export CONFIGSERVER_GIT_REPO="https://github.com/MindsIgnited/springcloudconfig"

CURRENT_DIR=$(pwd)
export CONFIGSERVER_GIT_BASEDIR=${CURRENT_DIR}/target/config


java ${JVM_OPTS} -jar ${PATH_TO_EXECUTABLE}/${CONFIG_APPLICATION}-${CONFIG_VERSION}.${CONFIG_APPLICATION_EXTENSION}
