#!/bin/bash

## MI Cloud Configuration Server

export KEYSTORE_PASSWORD=mindsignited
export KEYSTORE_ALIAS=mindsignited
export EUREKA_SERVER_URI=http://localhost:8761/micloudeureka
export SERVER_PORT=8888

if [ "$1" == "debug" ]; then
    mvn spring-boot:run -Drun.jvmArguments="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005" --debug
else
    mvn spring-boot:run $@
fi
